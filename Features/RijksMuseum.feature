Feature: RijksMuseum

    Scenario: Get RijksMuseum data
        #Happy flow
        Given I get data from URL: "https://www.rijksmuseum.nl/api/en/collection?key=0fiuZFh4&ps=10"        
        Then I should recieve more details for 1st member from URL: "https://www.rijksmuseum.nl/api/en/collection/{0}?key=0fiuZFh4"
        Then I should recieve details of tiles from URL: "https://www.rijksmuseum.nl/api/nl/collection/{0}/tiles?key=0fiuZFh4"
        #Unhappy flow
        Then I check if get data >=100 from URL: "https://www.rijksmuseum.nl/api/en/collection?key=0fiuZFh4&ps=101"  
        Then I check if the result page >=10000 from URL: "https://www.rijksmuseum.nl/api/en/collection?key=0fiuZFh4&ps=10&p=101"     
        #Format validation
        Then I check if object-number consists [0-9] from URL: "https://www.rijksmuseum.nl/api/en/collection?key=0fiuZFh4&ps=10"
        Then I check if language is correct of details from URL: "https://www.rijksmuseum.nl/api/en/collection/{0}?key=0fiuZFh4" 
      