# AH test automation assessment

## Candidate: Taisa Kholoshna

The application contains a series of automated tests to do GET operations of Rijksmuseum API,
with Cucumber and Node.js


## Installation
1. Install Node.js
2. Copy files to the desired directory.
3. Navigate to the folder in PowerShell 
4.  Init project
     ```bash
        npm install
    ```
5. (Optional) Install Cucumber
   ```bash
      npm install @cucumber/cucumber
    ```

## Usage

To run tests execute command: 
```bash
npm test 
OR
npx cucumber-js --require .\Step_definitions\  -f html:report/cucumber_report.html
```