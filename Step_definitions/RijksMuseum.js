const { Given, Then } = require('@cucumber/cucumber');
const httpHelper = require('../Helpers/HttpUtilities');
const assert = require('assert');
const regex = /\d/;
let objectNumber;


Given('I get data from URL: {string}', async function (url) {    
    var response = await httpHelper.get(url);    
    assert(response.data.count = 10);   
    objectNumber = response.data.artObjects[0].objectNumber; 
});

Then('I should recieve more details for 1st member from URL: {string}', async function (url) {
    var response = await httpHelper.get(url.replace('{0}', objectNumber));
    var title = response.data.artObject.title;
    var image = response.data.artObject.hasImage;
    assert(title.length > 0);
    assert(image = 'true')
});

Then('I should recieve details of tiles from URL: {string}', async function (url) {  
    var response = await httpHelper.get(url.replace('{0}', objectNumber));
    var tiles = response.data.levels[0].tiles;
    assert(tiles.length > 0);
});

Then('I check if get data >=100 from URL: {string}', async function (url) {    
    var response = await httpHelper.get(url);
    try{    
        assert.throws(async () => { await httpHelper.get(url)} ) 
    } catch (err) {
        console.log('Error')
    } 
});

Then('I check if the result page >=10000 from URL: {string}', async function (url) {    
    var response = await httpHelper.get(url);
    try{    
        assert.throws(async () => {await httpHelper.get(url)}) 
    } catch (err) {
        console.log('Error')
    } 
});

Then('I check if object-number consists [0-9] from URL: {string}', async function (url) {  
    var response = await httpHelper.get(url);    
    assert(response.data.count = 10);   
    objectNumber = response.data.artObjects[0].objectNumber; 
    containsNumber =  regex.test(objectNumber);
    assert(containsNumber = 'true');
});

Then ('I check if language is correct of details from URL: {string}', async function (url) {  
    var response = await httpHelper.get(url.replace('{0}', objectNumber));
    var language = response.data.artObject.language ;
    assert(language = 'nl');
});