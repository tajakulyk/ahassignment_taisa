const axios = require('axios');

module.exports.get = async function (url) {
    var response = await axios.get(url)
        .then((response) => {
            return response;
        })
        .catch((err) => {
            //console.log("AXIOS ERROR: ", err);
            throw err.response;
        });

    return response;
}

