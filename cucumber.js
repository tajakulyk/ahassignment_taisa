module.exports = { default: '--publish-quiet' }     

const cucumberJunitConvert = require('cucumber-junit-convert');

const options = {
    inputJsonFile: 'report/cucumber_report.json',
    outputXmlFile: 'rspec.xml',
    featureNameAsClassName: true // default: false
}

cucumberJunitConvert.convert(options);